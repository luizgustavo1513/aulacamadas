﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Aula_Camadas.Controllers
{
    [Route("/api/[Controller]")]
    [ApiController]
    public class PromoverController: ControllerBase
    {
        private IServPromover _servPromover;
        public PromoverController(IServPromover servPromover)
        {
            _servPromover = servPromover;
        }

        [HttpPost]
        public ActionResult Create(InserirPromoverDTO inserirPromover)
        {
            try
            {
                _servPromover.Create(inserirPromover);
                return Ok();
            }
            catch (Exception e)
            { 
                return BadRequest(e.Message);
            }
        }
    }
}
