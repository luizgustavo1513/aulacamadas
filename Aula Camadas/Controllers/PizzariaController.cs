﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Aula_Camadas
{
    [Route("api/[Controller]")]
    [ApiController]
    public class PizzariaController : ControllerBase
    {
        private IServPizzaria _servPizzaria;
        public PizzariaController(IServPizzaria servPizzaria)
        {
            _servPizzaria = servPizzaria;
        }

        [HttpPost]
        public ActionResult Inserir(InserirPizzariaDTO pizzaria) {
            try
            {
                _servPizzaria.Inserir(pizzaria);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public ActionResult Listar() {
            try
            {
                var pizzarias = _servPizzaria.findMany();

                var pizzariasFilter = pizzarias.Select(p => new
                {
                    Id=p.Id,
                    Nome = p.Nome,
                }).ToList();

                return Ok(pizzariasFilter);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                _servPizzaria.Delete(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
