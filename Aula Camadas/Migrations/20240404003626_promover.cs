﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Aula_Camadas.Migrations
{
    /// <inheritdoc />
    public partial class promover : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Promover",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Descricao = table.Column<string>(type: "TEXT", nullable: false),
                    Valor = table.Column<decimal>(type: "TEXT", nullable: false),
                    DataVigencia = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Status = table.Column<int>(type: "INTEGER", nullable: false),
                    CodigoPizzaria = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Promover", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Promover_Pizzaria_CodigoPizzaria",
                        column: x => x.CodigoPizzaria,
                        principalTable: "Pizzaria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Promover_CodigoPizzaria",
                table: "Promover",
                column: "CodigoPizzaria");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Promover");
        }
    }
}
