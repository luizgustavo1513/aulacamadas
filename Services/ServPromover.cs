﻿using Entities;
using Repositories;

namespace Services
{
    public interface IServPromover
    {
        void Create(InserirPromoverDTO inserirPromoverDTO);
    }

    public class ServPromover: IServPromover
    {
        private IRepoPromover _repoPromover;

        public ServPromover(IRepoPromover repoPromover)
        {
            _repoPromover = repoPromover;
        }

        public void Create(InserirPromoverDTO inserirPromoverDTO)
        {
            var promover = new Promover();

            promover.Descricao = inserirPromoverDTO.Descricao;
            promover.DataVigencia = inserirPromoverDTO.DataVigencia;
            promover.CodigoPizzaria = inserirPromoverDTO.CodigoPizzaria;
            promover.Valor = inserirPromoverDTO.Valor;

            _repoPromover.Create(promover);
        }

        public Promover FindFirst()
        {

        }
    }
}
