﻿using Entities;
using Repositories;

namespace Services
{
    public interface IServPizzaria
    { 
        void Inserir(InserirPizzariaDTO inserirPizzariaDTO);
        List<Pizzaria> findMany();
        void Delete(int id);
    }


    public class ServPizzaria : IServPizzaria
    {
        private IRepoPizzaria _repoPizzaria;

        public ServPizzaria(IRepoPizzaria repoPizzaria)
        {
            _repoPizzaria = repoPizzaria;

        }

        public void Inserir(InserirPizzariaDTO inserirPizzariaDTO)
        {
            var pizzaria = new Pizzaria();



            pizzaria.Nome = inserirPizzariaDTO.Nome;
            pizzaria.Endereco = inserirPizzariaDTO.Endereco;
            pizzaria.Telefone = inserirPizzariaDTO.Telefone;

            ValidateInserirPizzaria(pizzaria);

            _repoPizzaria.Inserir(pizzaria);

        }

        public void ValidateInserirPizzaria(Pizzaria pizzaria)
        {
            if(pizzaria.Nome.Length <40)
            {
                throw new Exception("Nome inválido. Deve possuir no mínimo 40 caractéres");
            }
        }

        public List<Pizzaria> findMany()
        {
            var pizzarias = _repoPizzaria.findMany();
            
            return pizzarias;
        }

        public void Delete(int id)
        {
            var pizzaria = _repoPizzaria.findMany().Where(p => p.Id == id).First();

            _repoPizzaria.Delete(pizzaria);
        }
    }
}
