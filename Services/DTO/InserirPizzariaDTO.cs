﻿namespace Services
{
    public class InserirPizzariaDTO
    {
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
    }
}
