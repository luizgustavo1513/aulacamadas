﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IRepoPromover
    {
        void Create(Promover promover);
    }
    public class RepoPromover: IRepoPromover
    {
        private DataContext _dataContext;

        public RepoPromover(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Create(Promover promover)
        {

            _dataContext.Add(promover);

            _dataContext.SaveChanges();

        }

        public Promover findFirst(int id)
        {
            var promover = _dataContext.Promover.First(p => p.Id == id);

            return promover;
        }

    }
}
