﻿using Entities;

namespace Repositories
{

    public interface IRepoPizzaria
    {
        void Inserir(Pizzaria pizzaria);
        List<Pizzaria> findMany();
        void Delete(Pizzaria pizzaria);
    }
    public class RepoPizzaria: IRepoPizzaria
    {
        private DataContext _dataContext;

        public RepoPizzaria(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Inserir(Pizzaria pizzaria)
        {
            _dataContext.Add(pizzaria);

            _dataContext.SaveChanges();
        }

        public List<Pizzaria> findMany() {
            var list = _dataContext.Pizzaria.ToList();

            return list;
        }

        public void Delete(Pizzaria pizzaria)
        {
            _dataContext.Remove(pizzaria);

            _dataContext.SaveChanges();
        }
    }
}
